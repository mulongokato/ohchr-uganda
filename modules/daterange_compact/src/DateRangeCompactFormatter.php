<?php

namespace Drupal\daterange_compact;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\daterange_compact\Entity\DateRangeCompactFormatInterface;

/**
 * Provides a service to handle compact formatting of date ranges.
 */
class DateRangeCompactFormatter implements DateRangeCompactFormatterInterface {

  /**
   * The config entity storage for compact date range formats.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $formatStorage;

  /**
   * The core date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $coreDateFormatter;

  /**
   * Constructs the compact date range formatter service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The core date formatter.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter) {
    $this->formatStorage = $entity_type_manager->getStorage('daterange_compact_format');
    $this->coreDateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public function formatDateRange(string $startDate, string $endDate, string $type = 'medium_date', $timezone = NULL, $langcode = NULL): FormattedDateTimeRange {
    $startTimestamp = (new DrupalDateTime($startDate, $timezone))->getTimestamp();
    $endTimestamp = (new DrupalDateTime($endDate, $timezone))->getTimestamp();
    return $this->formatTimestampRange($startTimestamp, $endTimestamp, $type, $timezone, $langcode);
  }

  /**
   * {@inheritdoc}
   */
  public function formatTimestampRange(int $start_timestamp, int $end_timestamp, string $type = 'medium_datetime', $timezone = NULL, $langcode = NULL): FormattedDateTimeRange {
    /** @var \Drupal\daterange_compact\Entity\DateRangeCompactFormatInterface $format */
    $format = $this->formatStorage->load($type);

    $patterns = $this->derivePatterns($format, $start_timestamp, $end_timestamp, $timezone);
    $start_pattern = $patterns['start_pattern'] ?: '';
    $end_pattern = $patterns['end_pattern'] ?: '';
    $separator = $patterns['separator'] ?: '';

    // Delegate the actual output to core's date formatting service.
    $start_text = $start_pattern ? $this->coreDateFormatter->format($start_timestamp, 'custom', $start_pattern, $timezone, $langcode) : '';
    $end_text = $end_pattern ? $this->coreDateFormatter->format($end_timestamp, 'custom', $end_pattern, $timezone, $langcode) : '';
    return ($start_text == $end_text)
      ? new FormattedDateTimeRange($start_text)
      : new FormattedDateTimeRange($start_text . $separator . $end_text);
  }

  /**
   * Derive patterns for the start and end dates according to their values.
   *
   * Examines the values of start and end in the context of the given timezone
   * and picks suitable patterns, falling back to default ones if necessary.
   *
   * @param \Drupal\daterange_compact\Entity\DateRangeCompactFormatInterface $format
   *   The compact date range format configuration entity.
   * @param int $start_timestamp
   *   A UNIX timestamp representing the start time.
   * @param int $end_timestamp
   *   A UNIX timestamp representing the end time.
   * @param string|null $timezone
   *   (optional) Time zone identifier, as described at
   *   http://php.net/manual/timezones.php Defaults to the time zone used to
   *   display the page.
   *
   * @return string[]
   *   An array with the following keys: start_pattern, end_pattern, separator.
   */
  private function derivePatterns(DateRangeCompactFormatInterface $format, $start_timestamp, $end_timestamp, $timezone) {
    $start_date_time = DrupalDateTime::createFromTimestamp($start_timestamp, $timezone);
    $end_date_time = DrupalDateTime::createFromTimestamp($end_timestamp, $timezone);

    $default_pattern = $format->get('default_pattern') ?: '';
    $default_separator = $format->get('default_separator') ?: '';

    // Strings containing the ISO-8601 representations of the start and end
    // datetime can be used to determine if the date and/or time are the same.
    $start_iso_8601 = $start_date_time->format('Y-m-d\TH:i:s');
    $end_iso_8601 = $end_date_time->format('Y-m-d\TH:i:s');

    if ($start_iso_8601 === $end_iso_8601) {
      // The start and end values are the same.
      return [
        'start_pattern' => $default_pattern,
        'end_pattern' => '',
        'separator' => '',
      ];
    }

    if (substr($start_iso_8601, 0, 10) == substr($end_iso_8601, 0, 10)) {
      // The range is contained within a single day.
      $start_pattern = $format->get('same_day_start_pattern') ?: '';
      $end_pattern = $format->get('same_day_end_pattern') ?: '';
      $separator = $format->get('same_day_separator') ?: $default_separator;
      if ($start_pattern || $end_pattern) {
        return [
          'start_pattern' => $start_pattern,
          'end_pattern' => $end_pattern,
          'separator' => ($start_pattern && $end_pattern) ? $separator : '',
        ];
      }
    }

    if (substr($start_iso_8601, 0, 7) === substr($end_iso_8601, 0, 7)) {
      // The range spans several days within the same month.
      $start_pattern = $format->get('same_month_start_pattern') ?: '';
      $end_pattern = $format->get('same_month_end_pattern') ?: '';
      $separator = $format->get('same_month_separator') ?: $default_separator;
      if ($start_pattern || $end_pattern) {
        return [
          'start_pattern' => $start_pattern,
          'end_pattern' => $end_pattern,
          'separator' => ($start_pattern && $end_pattern) ? $separator : '',
        ];
      }
    }

    if (substr($start_iso_8601, 0, 4) === substr($end_iso_8601, 0, 4)) {
      // The range spans several months within the same year.
      $start_pattern = $format->get('same_year_start_pattern') ?: '';
      $end_pattern = $format->get('same_year_end_pattern') ?: '';
      $separator = $format->get('same_year_separator') ?: $default_separator;
      if ($start_pattern || $end_pattern) {
        return [
          'start_pattern' => $start_pattern,
          'end_pattern' => $end_pattern,
          'separator' => ($start_pattern && $end_pattern) ? $separator : '',
        ];
      }
    }

    // Fallback: show the start and end dates in full using the default
    // pattern. This is the case if the range spans different years,
    // or if the other patterns are not specified.
    return [
      'start_pattern' => $default_pattern,
      'end_pattern' => $default_pattern,
      'separator' => $default_separator,
    ];
  }

}
